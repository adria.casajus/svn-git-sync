#!/usr/bin/env bash
# Script for converting author from SVN to Git, used as an argument for
# --authors-prog
# Example:
# "foo.bar" will be "Foo Bar <foo.bar@example.com>"
# "not-human" will be "not-human <not-human@example.com>"
set -e

EMAIL_DOMAIN=virgo-gw.it

username2name() {
    local username=$1
    local firstname=${username%%.*}
    local lastname=${username##*.}
    local name="${firstname~} ${lastname~}"
    if [[ "$firstname" == "$lastname" ]]; then
        local name="$firstname"
    fi
    local email="${username}@$EMAIL_DOMAIN"
    echo "$name <$email>"
}

username2name $1