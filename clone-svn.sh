#!/bin/bash

here=$(readlink -f $0)
here=$(dirname $here)

if [ "$#" -lt "1" ]; then
	echo 'Which repo?'
	exit 1
fi
repo=$1
echo $repo

mkdir -p $here/initial
(
cd $here/initial
git svn clone -A $here/authors --authors-prog $here/authors-auto.sh -s svn+ssh://acasajus@olsvn/var/svn/advconf/$repo
cd $repo
git remote add origin git@git.ligo.org:virgo-svn-mirror/$repo.git
git push -u origin master
)
