#!/bin/bash

here=$(readlink -f $0)
here=$(dirname $here)

echo "START sync at $(date)"
for repo in $here/repos/*
do
	echo "Processing $repo at $(date)"
	(
	cd	$repo
	git svn fetch -A $here/authors --authors-prog $here/authors-auto.sh
	git for-each-ref refs/remotes/origin/tags | cut -d / -f 5- |
	while read ref
	do
		if ! git tag | grep -q $ref; then
			echo New tag $ref
			git tag -a "$ref" -m 'Tag from svn' "refs/remotes/origin/tags/$ref"
			#git push origin ":refs/heads/tags/$ref"
			#git push origin tag "$ref"
		fi
	done
	if git branch -a | grep -q origin/trunk; then
		git merge origin/trunk
	else
		git merge git-svn
	fi
	git push origin master
	git push origin --tags
	)
done
echo "END sync at $(date)"
echo "----------------------------------"